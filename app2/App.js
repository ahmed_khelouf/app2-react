import { StatusBar } from 'expo-status-bar';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import * as Location from 'expo-location';
import React, { useState, useEffect } from 'react';
import MeteoActuelleComponent from './components/MeteoActuelle';
import MeteoForecastComponent from './components/MeteoForecast';
export default function App() {

  const [location, setLocation] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);

  useEffect(() => {
    (async () => {

      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
        setErrorMsg('Permission to access location was denied');
        return;
      }

      let location = await Location.getCurrentPositionAsync({});
      setLocation(location);

      getMeteo(location.coords.latitude, location.coords.longitude);
      getMeteoForecast(location.coords.latitude, location.coords.longitude);
    })();
  }, []);



  let text = 'Waiting..';
  if (errorMsg) {
    text = errorMsg;
  } else if (location) {
    text = JSON.stringify("Latitude :" + " " + location.coords.latitude + " " + "Longitude" + " " + location.coords.longitude);
  }

  const [meteo, setMeteo] = useState(null);

  const [meteoForecast, setMeteoForecast] = useState(null);


  const API_KEY = '8572322ee5ac965a89c0e9088b17f1ad';

  const getMeteo = async (latitude, longitude) => {
    try {
      const response = await fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&units=metric&appid=${API_KEY}`);
      const data = await response.json();
      setMeteo(data);
      console.log(data);
      console.log(meteo)
    } catch (error) {
      console.error(error);
    }
  };

  const getMeteoForecast = async (latitude, longitude) => {
    try {
      const response = await fetch(`https://api.openweathermap.org/data/2.5/forecast?lat=${latitude}&lon=${longitude}&units=metric&appid=${API_KEY}`);
      const data = await response.json();
      setMeteoForecast(data);
      console.log(data);
      console.log(meteo)
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    if (meteo, meteoForecast) {
      console.log(meteo);
      console.log('meteo forecast', meteoForecast);
    }
  }, [meteo]);


  return (
    <ScrollView>
      <View style={styles.container}>
        {/* <Text style={styles.paragraph}>{text}</Text> */}

        <MeteoActuelleComponent meteo={meteo} />
        <View>
          <Text style={styles.paragraph}>Prévisions météo </Text>
          <MeteoForecastComponent meteo={meteoForecast} />
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 30,
  },
  paragraph: {
    fontSize: 25,
    textAlign: 'center',
    fontWeight: 'bold',
    paddingTop: 30,
  },
});