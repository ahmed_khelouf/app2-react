import React from 'react';
import { StyleSheet, Text, View , Image} from 'react-native';

const MeteoActuelleComponent = ({ meteo }) => {
    if (!meteo) {
        return (
            <View style={styles.container}>
                <Text style={styles.paragraph}>Loading...</Text>
            </View>
        );
    }

    return (
        <View style={styles.container}>
            <Text style={styles.title}>Aujourd'hui</Text>
            <Text style={styles.name}>{meteo.name}</Text>
            <Text style={styles.temperature}>{meteo.main.temp}°C</Text>
            <Text style={styles.paragraph}>{meteo.weather[0].description}</Text>
            <Text style={styles.paragraph}><Image
                source={{ uri: `http://openweathermap.org/img/w/${meteo.weather[0].icon}.png` }}
                style={{ width: 100, height: 100 }}
            /></Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        marginTop: 20,
        alignItems: 'center',
        backgroundColor: 'skyblue',
        borderRadius: 30,
        padding: 20,
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 10,
    },
    paragraph: {
        fontSize: 18,
        textAlign: 'center',
    },
    temperature: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 10,
        color: 'red',
    },
    name: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 10,
    },
});

export default MeteoActuelleComponent;
