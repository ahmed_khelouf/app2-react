import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView } from 'react-native';
import { format } from 'date-fns';
import { fr } from 'date-fns/locale';



const MeteoForecastComponent = ({ meteo }) => {
    if (!meteo) {
        return (
            <View style={styles.container}>
                <Text style={styles.paragraph}>Loading...</Text>
            </View>
        );
    }

    const formatDate = (dateString) => {
        const date = new Date(dateString);
        return format(date, "EEEE d MMMM 'à' HH'h'", { locale: fr });
    };

    const meteoByDate = (listeMeteo) => {
        const tabMeteo = {}

        listeMeteo.list.forEach(meteo => {
            const date = new Date(meteo.dt_txt)
            const day = format(date, "EEEE d MMMM", { locale: fr })

            if (!tabMeteo[day]) {
                tabMeteo[day] = []
            }

            tabMeteo[day].push(meteo)
        });

        return tabMeteo
    }

    const test = meteoByDate(meteo)

    console.log(test)

    return (
        <View >
            {Object.keys(test).map((day, index) => (
                <View key={index} style={styles.container}>
                    <Text style={styles.title}>{day}</Text>
                    <ScrollView horizontal={true}>
                        <View style={{ flexDirection: 'row' }}>
                            {test[day].map((meteo, index) => (
                                <View key={index} style={{ padding: 10 }}>
                                    <Text>{format(new Date(meteo.dt_txt), "HH'h'")}</Text>
                                    <Image
                                        style={{ width: 50, height: 50 }}
                                        source={{
                                            uri: `http://openweathermap.org/img/wn/${meteo.weather[0].icon}.png`,
                                        }}
                                    />
                                    <Text>{Math.round(meteo.main.temp)}°C</Text>
                                </View>
                            ))}
                        </View>
                    </ScrollView>
                </View>
            ))}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        marginTop: 20,
        alignItems: 'center',
        backgroundColor: '#f0f0f0',
        borderRadius: 30,
        width: '100%',

    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 10,
        paddingTop: 10,
    },
    paragraph: {
        fontSize: 18,
        textAlign: 'center',
    },
});

export default MeteoForecastComponent;
